**Prerequisites:**
- Picom : install picom and create your picom config at ~/.config/picom/picom.config
- NetworkManager applet : this config uses nm-applet but you can change that to anything you like
- dmenu and slock : this config uses dmenu as default run launcher and vanilla slock as lock screen



**Install:**

If you are already using qtile, it is recommended to first backup your qtile config somewhere else.
After that ensure that there is no qtile directory in your ~/.config folder.
Then simply clone this repository at ~/.config.

**Enjoy!**
